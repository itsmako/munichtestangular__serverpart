import * as express from 'express';
import Property from '../models/property.interface';
import generateID from '../shared/generate_id.function';
class PropertyController {
    public listProperty = '/properties/:property_id/bookings';
    public bookProperty = '/properties/book';
    public router = express.Router();
    public bookingsRef = this.firestore.collection('Booked');
    constructor(private firestore) {
        this.intializeRoutes();
    }

    public intializeRoutes() {
        this.router.get(this.listProperty, this.getAllProperty);
        this.router.post(this.bookProperty, this.bookPropertyByUser);
    }

    getAllProperty = (request: express.Request, response: express.Response) => {
        const bookings: Property[] = [];
        this.bookingsRef.where('property_id', '==', request.params.property_id).get()
            .then(snapshot => {
                snapshot.forEach(doc => {
                    bookings.push(doc.data());
                });
                response.send(bookings);
            })
            .catch(err => {
                console.log('Error getting documents', err);
            });
    }
    bookPropertyByUser = (request: express.Request, response: express.Response) => {
        const dataID = generateID(10);
        this.bookingsRef.doc(dataID).set(request.body.book);
        response.send('200 OK');
    }
}

export default PropertyController;