import * as express from 'express';
import Property from '../models/property.interface';

class UserController {
    public listProperty = '/users/:user_id/bookings';
    public router = express.Router();
    private properties: Property[];
    public bookingsRef = this.firestore.collection('Booked');
    constructor(private firestore) {
        this.intializeRoutes();
    }

    public intializeRoutes() {
        this.router.get(this.listProperty, this.getAllByUser);
    }

    getAllByUser = (request: express.Request, response: express.Response) => {
        const bookings: Property[] = [];
        this.bookingsRef.where('user', '==', request.params.user_id).get()
            .then(snapshot => {
                snapshot.forEach(doc => {
                    bookings.push(doc.data());
                });
                response.send(bookings);
            })
            .catch(err => {
                console.log('Error getting documents', err);
            });
    }
}
export default UserController;