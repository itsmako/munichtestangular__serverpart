import Property from "./property.interface";
import User from "./user.interface";

interface Bookings {
    property: Property;
    user: User
}
export default Bookings;