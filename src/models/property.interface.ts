interface Property {
    id: string;
    property_id: string;
    property_name: string;
    city: string;
    user?: string;
}
export default Property;

