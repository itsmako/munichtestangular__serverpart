import App from './app';
import PropertyController from './controllers/property.controller';
import UserController from './controllers/user.controller';
import * as firebase from "firebase";

const config = {
    apiKey: "AIzaSyCCMZyQx0MxD_ayfWnJhOySpdSvNeB3vWo",
    authDomain: "personalportfolio-bceba.firebaseapp.com",
    databaseURL: "https://personalportfolio-bceba.firebaseio.com",
    projectId: "personalportfolio-bceba",
    storageBucket: "personalportfolio-bceba.appspot.com",
    messagingSenderId: "1011037670680"
};
const base = firebase.initializeApp(config);
const firestore = base.firestore();
const app = new App(
    [
        new PropertyController(firestore),
        new UserController(firestore),
    ],
    8000,
);

app.listen();